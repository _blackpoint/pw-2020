#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <time.h>

pthread_mutex_t *mug_mutex;
struct pub pub_simulation_data;


struct pub 
{
    int available_mugs;
    int clients_remaining;
    int drinking_time_limit;
};

void *client_task(void *id)
{
    int client_id = (intptr_t)id;
    short client_finished_drinking = 0;

    printf("Client with id %d enterd the pub.\n", client_id);

    while(!client_finished_drinking)
    {
        for (int i = 0; i < pub_simulation_data.available_mugs; i++)
        {
            if(pthread_mutex_trylock(&mug_mutex[i]))
            {
                continue;
            }
            else
            {
                printf("Client with id %d took mug with id %d and started drinking\n", client_id, i);

                short sleep_time = rand() % pub_simulation_data.drinking_time_limit + 1;
                sleep(sleep_time);

                printf("Client with id %d put back mug with id %d after %d seconds\n", client_id, i, sleep_time);

                pub_simulation_data.clients_remaining -= 1;
                printf("Client with id %d is leaving the pub\n", client_id);
                printf("Number of customers inside: %d\n", pub_simulation_data.clients_remaining);

                pthread_mutex_unlock(&mug_mutex[i]);
                client_finished_drinking = 1;
                break;
            }
            
        }
    }
}

int main()
{
    srand(time(NULL));
    // Input data
    int clients_init;
    int mugs;
    int drinking_time;

    // Threads stuff
    pthread_t *clients;

    printf("Enter number of customers: ");
    scanf("%d", &clients_init);
    printf("Enter number of mugs: ");
    scanf("%d", &mugs);
    printf("Enter max time of drinking: ");
    scanf("%d", &drinking_time);

    clients = malloc(clients_init * sizeof(pthread_t));
    mug_mutex = malloc(mugs * sizeof(pthread_mutex_t));

    pub_simulation_data.available_mugs = mugs;
    pub_simulation_data.clients_remaining = clients_init;
    pub_simulation_data.drinking_time_limit = drinking_time;

    for (int i = 0; i < mugs; i++)
    {
        if (pthread_mutex_init(&mug_mutex[i], NULL)) 
        {
            printf("Error while initializing mutex with id %d\n", i);
        }
    }

    for (int i = 0; i < clients_init; i++)
    {
        if (pthread_create(&clients[i], NULL, client_task, (void*) (intptr_t) i))
        {
            printf("Error while creating client with id %d\n", i);
        }
    }

    for (int i = 0; i < clients_init; i++)
    {
        pthread_join(clients[i], NULL);
    }

    for (int i = 0; i < mugs; i++)
    {
        if (pthread_mutex_destroy(&mug_mutex[i])) 
        {
            printf("Error while destroying mutex with id %d\n", i);
        }
    }

    printf("Bar is empty, closing.\n");
}