#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

struct counter_structure
{
    int counter;
};

pthread_mutex_t mutex;

void *addition_thread(void *c_struct) 
{
    struct counter_structure *tmp = (struct counter_structure*)c_struct;

    for (int i = 0; i < 100000; i++)
    {
        pthread_mutex_lock(&mutex);
        tmp->counter += 1;
        pthread_mutex_unlock(&mutex);
    }
}

void *subtraction_thread(void *c_struct) 
{
    struct counter_structure *tmp = (struct counter_structure*)c_struct;

    for (int i = 0; i < 100000; i++)
    {
        pthread_mutex_lock(&mutex);
        tmp->counter -= 1;
        pthread_mutex_unlock(&mutex);
    }
}


int main() 
{
    struct counter_structure c_struct = {0};
    pthread_t addition;
    pthread_t subtraction;
    int status;

    status = pthread_mutex_init(&mutex, NULL);
    if (status != 0)
    {
        printf("Error while initializing mutex\n");
    }

    status = pthread_create(&addition, NULL, addition_thread, &c_struct);
    if (status != 0)
    {
        printf("Error while crearing thread\n");
    }

    status = pthread_create(&subtraction, NULL, subtraction_thread, &c_struct);
    if (status != 0)
    {
        printf("Error while crearing thread\n");
    }

    pthread_join(addition, NULL);
    pthread_join(subtraction, NULL);
    pthread_mutex_destroy(&mutex);

    printf("Result: %d\n", c_struct.counter);

    return 0;
}