#include <stdio.h>
#include <omp.h>

void dynamic()
{
    int a = 7;
    double start;
    double end;

    start = omp_get_wtime();
    #pragma omp parallel for num_threads(7) firstprivate(a) schedule(dynamic)
    for (int i = 0; i < 100; i++)
    {
        //printf("Thread %d a=%d\n", omp_get_thread_num(), a);
        a++;
    }
    end = omp_get_wtime();

    printf("Procedure execution time: %f\n", end - start);
}

void dynamic_3()
{
    int a = 7;
    double start;
    double end;

    start = omp_get_wtime();
    #pragma omp parallel for num_threads(7) firstprivate(a) schedule(dynamic, 3)
    for (int i = 0; i < 100; i++)
    {
        //printf("Thread %d a=%d\n", omp_get_thread_num(), a);
        a++;
    }
    end = omp_get_wtime();

    printf("Procedure execution time: %f\n", end - start);
}

void staticc()
{
    int a = 7;
    double start;
    double end;

    start = omp_get_wtime();
    #pragma omp parallel for num_threads(7) firstprivate(a) schedule(static)
    for (int i = 0; i < 100; i++)
    {
        //printf("Thread %d a=%d\n", omp_get_thread_num(), a);
        a++;
    }
    end = omp_get_wtime();

    printf("Procedure execution time: %f\n", end - start);
}

void staticc_3()
{
    int a = 7;
    double start;
    double end;

    start = omp_get_wtime();
    #pragma omp parallel for num_threads(7) firstprivate(a) schedule(static, 3)
    for (int i = 0; i < 100; i++)
    {
        //printf("Thread %d a=%d\n", omp_get_thread_num(), a);
        a++;
    }
    end = omp_get_wtime();

    printf("Procedure execution time: %f\n", end - start);
}

int main()
{
    printf("Schedule dynamic:\n");
    dynamic();

    printf("---------------------------------------\n");
    printf("Schedule dynamic, portion size 3:\n");
    dynamic_3();

    printf("---------------------------------------\n");
    printf("Schedule static:\n");
    staticc();

    printf("---------------------------------------\n");
    printf("Schedule static, portion size 3:\n");
    staticc_3();
    return 0;
}
