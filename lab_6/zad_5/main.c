#include <stdio.h>
#include <omp.h>

int main() 
{
    double sum;
    sum = 0;
    int input;

    printf("Enter number: ");
    scanf("%d", &input);

    #pragma omp parallel for num_threads(4) shared(sum) schedule(dynamic)
    for (int i = 0; i < 500; i++)
    {
        printf("Thread %d, sum: %g\n", omp_get_thread_num(), sum);

        #pragma omp critical
        {
            sum += input * input;
        }
    }

    printf("Result: %g\n", sum);
    return 0;
}
