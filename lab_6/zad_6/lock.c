#include <stdio.h>
#include <omp.h>

int main() 
{
    double sum;
    sum = 0;
    int input;

    double start_time;
    double end_time;

    omp_lock_t lock;
    omp_init_lock(&lock);

    printf("Enter number: ");
    scanf("%d", &input);

    start_time = omp_get_wtime();
    #pragma omp parallel for num_threads(4) shared(sum) schedule(dynamic)
    for (int i = 0; i < 500; i++)
    {
        omp_set_lock(&lock);
        sum += input * input;
        omp_unset_lock(&lock);
    }
    end_time = omp_get_wtime();

    printf("Result: %g\n", sum);
    printf("Time: %f\n", end_time - start_time);
    return 0;
}
