#include <stdio.h>
#include <omp.h>

int main() 
{
    double sum;
    sum = 0;
    int input;

    double start_time;
    double end_time;

    printf("Enter number: ");
    scanf("%d", &input);

    start_time = omp_get_wtime();
    #pragma omp parallel for num_threads(4) shared(sum) schedule(dynamic)
    for (int i = 0; i < 500; i++)
    {
        #pragma omp critical
        {
            sum += input * input;
        }
    }
    end_time = omp_get_wtime();

    printf("Result: %g\n", sum);
    printf("Time: %f\n", end_time - start_time);
    return 0;
}
