#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <stdbool.h>

#define NUM_THREADS 3

#define N 4
#define M 3
#define P 2

// Declarations
void generate_data(size_t s1, size_t s2, int matrix[s1][s2], bool fill_with_zeros);
void display_matrix(size_t s1, size_t s2, int matrix[s1][s2]);
void multiply(size_t col_rows, size_t rows, size_t cols, int matrix1[rows][col_rows], int matrix2[col_rows][cols], int output[cols][rows]);

int main()
{
    srand(time(NULL));

    int matrix1[N][M];
    int matrix2[M][P];

    // Fill matrixes with random numbers
    generate_data(N, M, matrix1, false);
    generate_data(M, P, matrix2, false);

    printf("Matrix 1:\n");
    display_matrix(N, M, matrix1);
    printf("Matrix 2:\n");
    display_matrix(M, P, matrix2);

    int multiplication_result[N][P];
    // Fill matrix with zeros
    generate_data(N, P, multiplication_result, true);

    double start_time = omp_get_wtime();
    multiply(M, N, P, matrix1, matrix2, multiplication_result);
    double end_time = omp_get_wtime();

    printf("Multiplication finished in %f s, with result:\n", end_time - start_time);
    display_matrix(N, P, multiplication_result);
}

void multiply(size_t col_rows, size_t rows, size_t cols, int matrix1[rows][col_rows], int matrix2[col_rows][cols], int output[rows][cols])
{
    size_t j_1 = 0;

    #pragma omp parallel for num_threads(NUM_THREADS) // NUM_THREADS = 7
    for (size_t k = 0; k < rows; k++)
    {
        for (size_t j = 0; j < cols; j++)
        {
            for (size_t i = 0; i < col_rows; i++)
            {
                output[k][j] += matrix1[k][i] * matrix2[i][j_1];
            }
            j_1++;
        }
        j_1 = 0;
    }
}

void generate_data(size_t s1, size_t s2, int matrix[s1][s2], bool fill_with_zeros)
{
    for (size_t i = 0; i < s1; i++)
    {
        for (size_t j = 0; j < s2; j++)
        {
            matrix[i][j] = fill_with_zeros ? 0 : rand() % 100;
        }
    }
}

void display_matrix(size_t s1, size_t s2, int matrix[s1][s2])
{
    for (size_t i = 0; i < s1; i++)
    {
        for (size_t j = 0; j < s2; j++)
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}