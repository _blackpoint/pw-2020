#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define MULTITHREAD

int fibonacci_function(int n) 
{ 
    int i;
    int j;
    if (n <= 1) 
        return n;
    
    #ifdef MULTITHREAD
        #pragma omp task shared(i)
    #endif
    i = fibonacci_function(n-1);

    #ifdef MULTITHREAD
        #pragma omp task shared(j)
    #endif
    j = fibonacci_function(n-2);

    return i + j; 
} 

int main(int argc, char* argv[])
{
    int input;
    if (argc == 2)
    {
        input = atoi(argv[1]);
    }
    else
    {
        input = 5;
    }
    
    double start_time = omp_get_wtime();
    int result = fibonacci_function(input);
    double end_time = omp_get_wtime();

    #ifdef MULTITHREAD
        printf("Warning - multithreading enabled.\n");
    #endif
    printf("fibonacci_function(%d) = %d\n", input, result);
    printf("Calculations time: %f s\n", end_time - start_time);
}