#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

#include "../include/vectors_util.h"

#define VECTOR_SIZE 1000000000

// Control flags
// #define SHOW_OUTPUT

int main(int argc, char* argv[])
{
    size_t size;

    if (argc == 2)
    {
        size = atoi(argv[1]);
    }
    else
    {
        size = VECTOR_SIZE;
    }
    
    printf("Vectors' size: %ld\n", size);
    srand(time(NULL));

    double start_time = omp_get_wtime();
    double *vector1 = malloc(sizeof(double) * size);
    double *vector2 = malloc(sizeof(double) * size);
    double end_time = omp_get_wtime();
    printf("Memory allocation time: %f s.\n", end_time - start_time);

    start_time = omp_get_wtime();
    vector_generate(vector1, size);
    vector_generate(vector2, size);
    end_time = omp_get_wtime();
    printf("Vector data generation time: %f s.\n", end_time - start_time);

    #ifdef SHOW_OUTPUT
        vector_display(vector1, size);
        vector_display(vector2, size);
    #endif

    start_time = omp_get_wtime();
    double product = vector_multiply(vector1, vector2, size);
    end_time = omp_get_wtime();

    printf("Scalar product of vector1 and vector2: %.0f\n", product);
    printf("Calculation time: %f s.\n", end_time - start_time);

    free(vector1);
    free(vector2);
}