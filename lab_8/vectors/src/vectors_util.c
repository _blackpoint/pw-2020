#include "../include/vectors_util.h"

#include <time.h>
#include <stdio.h>

// Control flags
// #define MULTITHREAD
#define MULTITHREAD_SIMD

void vector_generate(double* vector, size_t size)
{
    for (int i = 0; i < size; i++)
    {
        vector[i] = rand() % 5;
    }
}

void vector_display(double* vector, size_t size)
{
    printf("Vector with size %ld: ", size);

    for (int i = 0; i < size; i++)
    {
        printf("%.0f ", vector[i]);
    }
    printf("\n");
}

double vector_multiply(double* vector1, double *vector2, size_t size)
{
    double result = 0;

    #ifdef MULTITHREAD
        printf("Warning - Multithreading enabled.\n");
        #pragma omp parallel for num_threads(7) shared(result)
    #endif
    #ifdef MULTITHREAD_SIMD
        printf("Warning - SIMD enabled.\n");
        #pragma omp simd
    #endif

    for (int i = 0; i < size; i++)
    {
        result += vector1[i] * vector2[i];
    }
    return result;
}