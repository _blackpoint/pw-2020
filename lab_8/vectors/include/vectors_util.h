#include <stdlib.h>

void vector_generate(double* vector, size_t size);
void vector_display(double* vector, size_t size);
double vector_multiply(double* vector1, double *vector2, size_t size);