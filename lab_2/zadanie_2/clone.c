#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sched.h>

#include "pomiar_czasu/pomiar_czasu.h"

int zmienna_globalna = 0;

#define ROZMIAR_STOSU 1024 * 64

#define CSIGNAL    	  0x000000ff	/* signal mask to be sent at exit */
#define CLONE_VM      0x00000100	/* set if VM shared between processes */
#define CLONE_FS      0x00000200	/* set if fs info shared between processes */
#define CLONE_FILES   0x00000400	/* set if open files shared between processes */
#define CLONE_SIGHAND 0x00000800	/* set if signal handlers shared */
#define CLONE_PID     0x00001000	/* set if pid shared */

int funkcja_watku(void *argument)
{
    zmienna_globalna++;

    int wynik;
    wynik = execv("./program", NULL);
    if (wynik == -1)
      printf("Proces potomny nie wykonal programu\n");

    return 0;
}

int main()
{
    void *stos;
    pid_t pid;

    stos = malloc(ROZMIAR_STOSU);
    if (stos == 0)
    {
        printf("Proces nadrzędny - blad alokacji stosu\n");
        exit(1);
    }

    inicjuj_czas();

    for (int i = 0; i < 1000; i++)
    {
        pid = clone(&funkcja_watku, (void *)stos + ROZMIAR_STOSU,
                    CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM, 0);

        waitpid(pid, NULL, __WCLONE);
    }

    drukuj_czas();

    free(stos);
}
