package zad2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

class HistogramThreadTask extends Thread {
    private int id;
    private Map<Character, Integer> histogramData;
    private String imageDataDictionary;

    private char[][] imageData;
    private int width;
    private int height;

    public HistogramThreadTask(int id, char[][] imageData, int width, int height, String imageDataDictionary) {
        this.id = id;
        this.imageData = imageData;
        this.width = width;
        this.height = height;
        this.imageDataDictionary = imageDataDictionary;
        this.histogramData = new HashMap<>();
    }

    /**
     * This procedure calculates histogram data for characters passed into
     * constructor (inameDataDictionary).
     */
    @Override
    public void run() {
        System.out.println("[Thread " + this.id + "] Starting. Data part to count: " + this.imageDataDictionary);

        int counter;

        for (int i = 0; i < this.imageDataDictionary.length(); i++) {
            counter = 0;
            for (int j = 0; j < this.height; j++) {
                for (int k = 0; k < this.width; k++) {
                    if (this.imageData[j][k] == this.imageDataDictionary.charAt(i)) {
                        counter++;
                    }
                }
            }
            this.histogramData.put(this.imageDataDictionary.charAt(i), counter);
        }
    }

    public Map<Character, Integer> getHistogramData() {
        return this.histogramData;
    }
}

public class FakeImage {
    private int width;
    private int height;

    private String imageDataDictionary;

    private char[][] imageData;

    private Map<Character, Integer> histogramData;

    public FakeImage(int width, int height) {
        this.width = width;
        this.height = height;

        this.imageData = new char[height][width];
    }

    /**
     * This procedure is filling imageData with random randomly picked characters
     * from availableColors.
     * 
     * @param availableColors String based on which data is generated.
     */
    public void fillWithRandomData(String availableColors) {
        if (!StringUtil.areCharactersUnique(availableColors)) {
            throw new IllegalArgumentException("All characters must be unique");
        }
        Random randomGenerator = new Random();
        this.imageDataDictionary = availableColors;

        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                imageData[i][j] = availableColors.charAt(randomGenerator.nextInt(availableColors.length()));
            }
        }
    }

    /**
     * This methods creates a buch of threads, which calculates histogram of
     * FakeImage. The number of threads depends on length of String passed to
     * fillWithRandomData procedure. Maximum number of threads is specified via
     * parameter of this function.
     * 
     * @param maxNumberOfThreads Maximum number of threads created by procedure
     */
    public void calculateHistogram(int maxNumberOfThreads) {
        int numberOfDictElements = imageDataDictionary.length();
        int numberOfThreads;

        if (numberOfDictElements > maxNumberOfThreads) {
            numberOfThreads = maxNumberOfThreads;
        } else {
            numberOfThreads = numberOfDictElements;
        }

        String[] parts = StringUtil.divideString(this.imageDataDictionary, numberOfThreads);

        HistogramThreadTask[] threads = new HistogramThreadTask[numberOfThreads];
        Map<Character, Integer> histogramData = new HashMap<>();

        for (int i = 0; i < numberOfThreads; i++) {
            (threads[i] = new HistogramThreadTask(i, this.imageData, this.width, this.height, parts[i])).start();
        }

        for (int i = 0; i < numberOfThreads; i++) {
            try {
                threads[i].join();
                histogramData.putAll(threads[i].getHistogramData());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.histogramData = histogramData;
    }

    public void displayHistogram() {
        for (Map.Entry<Character, Integer> entry : this.histogramData.entrySet()) {
            System.out.println("Character: " + entry.getKey() + ", occurrences: " + entry.getValue());
        }
    }

    public void displayImage() {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                System.out.print(imageData[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}