package zad2;

public class App {
    public static void main(String[] args) {

        // Maximum number of threads which will be created by program
        int maxNumberOfThreads = 100;
        // Data which will be used for image generation
        String dictionary = "!@#$;'%^&*";

        FakeImage fakeImage = new FakeImage(1920, 1080);

        fakeImage.fillWithRandomData(dictionary);
        System.out.println("Image with size: " + fakeImage.getWidth() + "x" + fakeImage.getHeight());
        System.out.println("Image contains following chars: " + dictionary);
        // fakeImage.displayImage();

        long startTime = System.nanoTime();
        fakeImage.calculateHistogram(maxNumberOfThreads);
        long endTime = System.nanoTime();
        fakeImage.displayHistogram();

        System.out.println("Calculations time: " + (endTime - startTime) / 1000000 + " ms.");
    }
}