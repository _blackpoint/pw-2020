package zad2;

import java.util.Arrays;

/**
 * Utility class for String operations.
 */
public class StringUtil {

    /**
     * This function divides String to several parts, defined by 'parts' parameter.
     * 
     * @param input Input string
     * @param parts Number of parts string should be divided into.
     * @return
     */
    public static String[] divideString(String input, int parts) {
        if (parts > input.length()) {
            throw new IllegalArgumentException("Cannot divide String to more parts than it's lenghth");
        }

        String[] output = new String[parts];
        int partLength = input.length() / parts;

        int counter = 0;
        for (int i = 0; i < input.length(); i += partLength) {
            if (counter == parts - 1 && input.length() % parts != 0) {
                output[counter] = input.substring(i, i + partLength + input.length() % parts);
                break;
            }
            output[counter] = input.substring(i, i + partLength);
            counter++;
        }

        return output;
    }

    /**
     * This function checks if all characters in given String are unique.
     * 
     * @param input String to be validated
     */
    public static boolean areCharactersUnique(String input) {
        char[] charArray = input.toCharArray();

        Arrays.sort(charArray);

        for (int i = 0; i < charArray.length - 1; i++) {
            if (charArray[i] != charArray[i + 1]) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }
}