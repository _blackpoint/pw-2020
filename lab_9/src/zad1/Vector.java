package zad1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

/**
 * Class implementing bacis mathematical vector.
 * @param <T> Type of vector's elements
 */
public class Vector<T> implements Iterable<T> {

    private final int length;

    private final List<T> vectorElements;

    public Vector(final int length) {
        this.length = length;
        this.vectorElements = new ArrayList<T>(length);
    }

    public int getLength() {
        return length;
    }

    public T getElement(final int index) {
        return this.vectorElements.get(index);
    }

    public void setElement(final int index, final T value) {
        this.vectorElements.set(index, value);
    }

    public void fillWith(T value) {
        for (int i = 0; i < this.length; i++) {
            this.vectorElements.add(value);
        }
    }

    /**
     * This function fills vector with data specified by given Function.
     * @param creator Function object used to generate values
     */
    public void fillWithRandomData(Function<Random, T> creator) {
        Random random = new Random();
        for (int i = 0; i < this.length; i++) {
            this.vectorElements.add(creator.apply(random));
        }
    }

    @Override
    public Iterator<T> iterator() {
        return this.vectorElements.iterator();
    }
}