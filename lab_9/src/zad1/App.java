package zad1;

class ThreadTask extends Thread {
    private final int id;

    private final Vector<Integer>[] vectors;
    private final int lowerBound;
    private final int upperBound;

    private Vector<Integer> outVector;

    public ThreadTask(int id, int lowerBound, int upperBound, Vector<Integer>[] vectors, Vector<Integer> outVector) {
        this.id = id;
        this.vectors = vectors;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.outVector = outVector;

        System.out.println("[Thread " + id + "] Starting...");
    }

    public void run() {
        for (int i = this.lowerBound; i < this.upperBound; i++) {
            this.outVector.setElement(i, vectors[0].getElement(i) + vectors[1].getElement(i));
        }
    }
}

public class App {
    public static void main(final String[] args) {
        // Config variables
        final int vectorsSize = 102400;
        final int upperRandomBound = 150;
        final int numberOfThreads = 2;

        if (numberOfThreads > vectorsSize) {
            System.out.println("Incorrect input values.");
            return;
        }

        @SuppressWarnings("unchecked")
        final Vector<Integer>[] vectors = new Vector[2];

        for (int i = 0; i < 2; i++) {
            vectors[i] = new Vector<Integer>(vectorsSize);
            vectors[i].fillWithRandomData(rand -> rand.nextInt(upperRandomBound));
        }

        System.out.println("Vector 1:");
        for (final Integer i : vectors[0]) {
            System.out.print(i + " ");
        }

        System.out.println("\nVector 2:");
        for (final Integer i : vectors[1]) {
            System.out.print(i + " ");
        }
        System.out.println("");

        ThreadTask[] threads = new ThreadTask[numberOfThreads];
        int chunkSize = vectorsSize / numberOfThreads;
        int modulo = vectorsSize % numberOfThreads;
        int pointer = 0;

        Vector<Integer> resultVector = new Vector<>(vectorsSize);
        resultVector.fillWith(0);

        for (int i = 0; i < numberOfThreads; i++) {
            if (modulo > 0) {
                (threads[i] = new ThreadTask(i, pointer, pointer + chunkSize + 1, vectors, resultVector)).start();
                pointer += chunkSize + 1;
                modulo -= 1;
            } else {
                (threads[i] = new ThreadTask(i, pointer, pointer + chunkSize, vectors, resultVector)).start();
                pointer += chunkSize;
            }

        }

        for (int i = 0; i < numberOfThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
            }
        }

        System.out.println("Sum of vector1 and vector2: ");
        for (final Integer i : resultVector) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}