package zad3;

/**
 * Implementation of Eratosthenes sieve alghrithm.
 */
class Eliminator extends Thread {
    private int id;
    private int[] data;
    private Range range;

    public Eliminator(int id, int[] inputArray, Range range) {
        this.id = id;
        this.data = inputArray;
        this.range = range;
    }

    /**
     * This procedure implements Eratosthenes sieve alghrithm. It works on the given
     * range of data array, passed into constructor.
     */
    public void eliminateMultiple() {
        System.out.println("[Thread " + this.id + "] Range: " + this.range.getFrom() + " to " + this.range.getTo());

        synchronized (this.data) {
            for (int i = 1; i < data.length; i++) {
                if (data[i] == 0)
                    continue; // Number is already eliminated

                for (int j = this.range.getFrom(); j < this.range.getTo(); j++) {
                    if (data[i] == data[j])
                        continue;
                    if (data[j] % data[i] == 0)
                        data[j] = 0;
                }
            }
        }

    }

    @Override
    public void run() {
        eliminateMultiple();
    }
}