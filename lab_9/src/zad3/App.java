package zad3;


public class App {
    public static void main(String[] args) {
        // Config variables
        int arrSize = 1000;
        int numberOfThreads = 9;

        if (args.length > 0) {
            arrSize = Integer.parseInt(args[0]);
        }
        if (args.length == 2) {
            numberOfThreads = Integer.parseInt(args[1]);
        }

        if (numberOfThreads > arrSize) {
            System.out.println("Incorrect input values " + numberOfThreads + " " + arrSize);
            return;
        }

        int[] data = new int[arrSize];
        int chunkSize = arrSize / numberOfThreads;

        for (int i = 0; i < arrSize; i++) {
            data[i] = i + 1;
        }

        Eliminator[] threads = new Eliminator[numberOfThreads];
        Range[] ranges = new Range[numberOfThreads];
        int counter = 0;

        System.out.println("Array size: " + arrSize);
        System.out.println("Number of threads: " + numberOfThreads);

        long startTime = System.nanoTime();
        for (int i = 0; i < numberOfThreads; i++) {
            if (i == numberOfThreads - 1 && arrSize % numberOfThreads != 0) {
                chunkSize += arrSize % numberOfThreads;
            }

            // Calculate range on which thread will work
            ranges[i] = new Range(counter, counter + chunkSize);
            counter += chunkSize;

            // Start thread
            (threads[i] = new Eliminator(i, data, ranges[i])).start();
        }

        for (int i = 0; i < numberOfThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long endTime = System.nanoTime();

        for (int i = 0; i < data.length; i++) {
            if (data[i] != 0) {
                System.out.print(data[i] + " ");
            }
        }

        System.out.println("\nCalculations time: " + ((endTime - startTime) / 1000000) + " ms");
    }

}