#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include <mpi/mpi.h>

#define NUMBER_OF_TRANSMISSIONS 10000

typedef struct ClientData 
{
    int id;
    char name[50];
    char surname[70];
    char email[100];
    char adress[150];
} client_data;

void init_data(struct ClientData *client_data);

int main(int argc, char* argv[])
{
    client_data cd;

    int number_of_processes;
    int rank;
        
    MPI_Init(&argc, &argv);
    
    double start_time = MPI_Wtime();
	MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (number_of_processes != 2)
    {
        fprintf(stderr, "Error - program requires 2 processes to run.\n");
        return -1;
    }

    int blocklengths[5] = {1, 50, 70, 100, 150};
    MPI_Datatype types[5] = {MPI_INT, MPI_CHAR, MPI_CHAR, MPI_CHAR, MPI_CHAR};
    MPI_Datatype mpi_client_data_type;
    MPI_Aint offsets[5];
    
    offsets[0] = offsetof(client_data, id);
    offsets[1] = offsetof(client_data, name);
    offsets[2] = offsetof(client_data, surname);
    offsets[3] = offsetof(client_data, email);
    offsets[4] = offsetof(client_data, adress);

    MPI_Type_create_struct(5, blocklengths, offsets, types, &mpi_client_data_type);
    MPI_Type_commit(&mpi_client_data_type);


    if (rank == 0) 
    {
        init_data(&cd);
        const int dest = 1;
        const int tag = 13;
        for (int i = 0; i < NUMBER_OF_TRANSMISSIONS; i++)
        {
    		MPI_Send(&cd, 1, mpi_client_data_type, dest, tag, MPI_COMM_WORLD);
            printf("[Rank %d] Structure was sent\n", rank);
        }
    }

    else if (rank == 1) 
    {
        MPI_Status status;
        const int src = 0;
        const int tag = 13;
		client_data received_data;

        for (int i = 0; i < NUMBER_OF_TRANSMISSIONS; i++)
        {
		    MPI_Recv(&received_data, 1, mpi_client_data_type, src, tag, MPI_COMM_WORLD, &status);
            printf("[Rank %d] Recived struct. Name: %s, surname: %s\n", rank, received_data.name, received_data.surname);
        }
    }

    MPI_Type_free(&mpi_client_data_type);
    double end_time = MPI_Wtime();
	MPI_Finalize();

    if (rank == 0)
    {
        printf("[Rank %d] Transfer complete in %f s.\n", rank, end_time - start_time);
    }
}

void init_data(struct ClientData *client_data)
{
    strcpy(client_data->name, "Karol");
    strcpy(client_data->surname, "Wojtyla");
    strcpy(client_data->email, "k.wojtyla@gmail.com");
    strcpy(client_data->adress, "ul. Krakowska 54, 30-540 Kraków");
}