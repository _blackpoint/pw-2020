#include <stdio.h>
#include <stdlib.h>
#include <mpi/mpi.h>

int get_odd_number(size_t index)
{
	int number = 1;

	for (size_t i = 0; i < index; i++)
	{
		number += 2;
	}

	return number;
}

int main(int argc, char *argv[])
{

	int number_of_processes;
	int rank;

	int root_rank = 0;

	size_t number_of_elements = argc > 1 ? atoi(argv[1]) : 5;
	if (number_of_elements < 1)
	{
		fprintf(stderr, "Incorrect number of processes: %ld\n", number_of_elements);
		exit(-1);
	}

	MPI_Init(&argc, &argv);
		
	MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// if (number_of_processes > number_of_elements)
	// {
	// 	fprintf(stderr, "Number of processes must be greater or equal to number of elements in series.\n");
	// 	exit(-1);
	// }

	size_t chunk_size = number_of_elements / number_of_processes;
	size_t modulo = number_of_elements % number_of_processes;

	double start = MPI_Wtime();

	double localsum = 0;
	double globalsum = 0;
	int sign;

	for (int i = rank; i < chunk_size * number_of_processes + modulo; i += number_of_processes ) 
	{
	    sign = (i % 2) == 0 ? 1 : -1;
	    localsum += sign * (1.0/(get_odd_number(i)));
	}

	MPI_Reduce(&localsum, &globalsum, 1, MPI_DOUBLE, MPI_SUM, root_rank, MPI_COMM_WORLD );
	globalsum *= 4;

	double end = MPI_Wtime();

	if(!rank){
		printf("======= Completed in: %lf s ========\n", end - start);
		printf("\tNumber of processes: %d\n", number_of_processes);
		printf("\tNumber of series elements: %ld\n", number_of_elements);
		printf("\tCalculated PI value: %lf\n", globalsum);
	}
	
	MPI_Finalize();
	return 0;
}
