/**
 * 
 * Compile with mpicc zad2.c -lm
 * 
*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi/mpi.h>
#include <math.h>


int main(int argc, char *argv[])
{

	int number_of_processes;
	int rank;

	int root_rank = 0;

	size_t number_of_elements = argc > 1 ? atoi(argv[1]) : 5;
	if (number_of_elements < 1)
	{
		fprintf(stderr, "Incorrect number of processes: %ld\n", number_of_elements);
		exit(-1);
	}

	MPI_Init(&argc, &argv);
		
	MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (number_of_processes != 4)
	{
		fprintf(stderr, "Number of processes must be equal to 4.\n");
		exit(-1);
	}

	size_t chunk_size = number_of_elements / number_of_processes;
	size_t modulo = 0;

    if (rank == number_of_processes - 1)
        modulo = number_of_elements % number_of_processes;

	double start = MPI_Wtime();

	double localsum = 0;
	double globalsum = 0;

	for (int i = rank * chunk_size + 1; i < (rank + 1) * chunk_size + modulo + 1; i++) 
	{
	    localsum += (1.0/(i));
        // printf("[Rank %d] Range: %d - %ld Local sum: %lf\n", rank, i, (rank + 1) * chunk_size + modulo , localsum);
        // localsum += 1;
	}

	MPI_Reduce(&localsum, &globalsum, 1, MPI_DOUBLE, MPI_SUM, root_rank, MPI_COMM_WORLD );
    globalsum -= log(number_of_elements);

	double end = MPI_Wtime();

	if(!rank){
		printf("======= Completed in: %lf s ========\n", end - start);
		printf("\tNumber of processes: %d\n", number_of_processes);
		printf("\tNumber of series elements: %ld\n", number_of_elements);
		printf("\tCalculated Gn value: %lf\n", globalsum);
	}
	
	MPI_Finalize();
	return 0;
}
