#!/usr/bin/bash

counter=1
while [ $counter -le 10000 ]; do
    mpirun -np 4 ./zad2.out $counter
    let counter=counter*10
done
