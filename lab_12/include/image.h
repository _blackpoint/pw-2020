#ifndef PBM_H
#define PBM_H

#include <string>

#define MSG_LEN               20
#define HEADER_LEN            10
#define TYPE_LEN              2

class Image
{
    public:
        // Image(int width, int height, int maxValue, unsigned char *pixel, unsigned char type[3]);
        Image();
        void open(std::string uri);
        void save(std::string uri);
        void applyFilter();

        // Getters
        int getHeight();
        int getWidth();
        std::string getFileName();

    private:
        int width;
        int height;
        int maxValue;
        std::string fileName;
        unsigned char *pixel;
        unsigned char type[3];

        // Internal IO procedures
        void readInput(FILE *imageFile);
        void writeData(FILE *outFile);

};


#endif