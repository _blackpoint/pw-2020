#!/usr/bin/bash

# Build project with 'make DEBUG=0' first
counter=1
while [ $counter -le 20 ]; do
	echo === Number of processes: $counter ===
	mpirun -np $counter ./bin/PGMFilterApp casablanca.pgm | grep 'Total time elapsed:\|Amdahl'
	let counter=counter+1
done
