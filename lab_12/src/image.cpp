#include "../include/image.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include <mpi/mpi.h>

Image::Image()
{

}

void Image::open(std::string uri)
{
    FILE *imageFile = fopen(uri.c_str(), "rb");
    if(imageFile == 0) 
    {
        throw std::runtime_error("File: " + uri + " couldn't be opened.");
	}

	this->fileName = uri;
	readInput(imageFile);
    fclose(imageFile);
}

void Image::save(std::string uri)
{
	FILE *outFile = fopen(uri.c_str(), "wb+");
	if(outFile == 0) 
    {
        throw std::runtime_error("File: " + uri + " couldn't be opened.");
	}

	writeData(outFile);
	fclose(outFile);
}

void Image::readInput(FILE *imageFile) 
{
	/* reads the header of the file to see the format */
	char file_header[HEADER_LEN];
	fgets(file_header, HEADER_LEN, imageFile);

	/* sets the type of the image */
	memcpy(this->type, file_header, TYPE_LEN);
	this->type[2] = '\0';
	if(memcmp(this->type, "P5", TYPE_LEN) != 0) 
    {
        throw std::runtime_error("Header of file is incorrect.");
	}

	/* reads width and height of the image */
	if(fscanf(imageFile, "%d %d\n", &(this->width), &(this->height)) != 2) 
    {
        throw std::runtime_error("Height of width of the image was not read correctly.");
	}

	this->pixel = (unsigned char*) malloc(sizeof(unsigned char) * this->height  * this->width);

	/* reads maxValue */
	if(fscanf(imageFile, "%d\n", &(this->maxValue)) != 1) 
    {
        throw std::runtime_error("The height and the width were not red corectly");
	}

	/* reading the image content */
	if(memcmp(this->type, "P5", TYPE_LEN) == 0) 
    {
		fread(this->pixel, this->width, this->height, imageFile);
	}
}

void Image::writeData(FILE *outFile) 
{
	/* wrinting the type of the image */
	fprintf(outFile, "%s\n", this->type);

	/* writing the width and the height of the image */
	fprintf(outFile, "%d %d\n", this->width, this->height);

	/* writng the maxValue*/
	fprintf(outFile, "%d\n", this->maxValue);

	/* write data back to file */
	fwrite(this->pixel, sizeof(unsigned char), this->height * this->width, outFile);
	
    free(this->pixel);
}

void Image::applyFilter()
{
	int numberOfProcesses;
	int rank;
	
	int matrix[3][3] = {
		{1, 2, 1},
		{2, 4, 2},
		{1, 2, 1}
	};

	size_t arr_length = this->height  * this->width;

	unsigned char *data_copy = (unsigned char*) malloc(sizeof(unsigned char) * arr_length);
	memcpy(data_copy, this->pixel, arr_length * sizeof(unsigned char));

	MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcesses);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	size_t numberOfRows = arr_length / this->width;
	size_t fromRow = numberOfRows / numberOfProcesses * rank;
	size_t toRow = fromRow + (numberOfRows / numberOfProcesses);

	if (rank == 0)
		fromRow += 1;

	if (rank == numberOfProcesses - 1)
	{
		if (numberOfRows % numberOfProcesses != 0)
			toRow += numberOfRows % numberOfProcesses;
	}

	std::pair<size_t, size_t> range(fromRow, toRow);

	double timeElapsed = 0;
	if (rank != 0)
	{
		MPI_Status status;
		MPI_Recv(this->pixel, (fromRow-1) * this->width, MPI_UNSIGNED_CHAR, rank-1, 17, MPI_COMM_WORLD, &status);
		MPI_Recv(&timeElapsed, 1, MPI_DOUBLE, rank-1, 0, MPI_COMM_WORLD, &status);
	}



	double startTime = MPI_Wtime();
	for (size_t i = range.first * this->width; i < toRow * this->width; i++)
	{
		if ((i % this->width == 0) || ((i + 1) % this->width == 0)) continue;
		this->pixel[i] = 
			(matrix[0][0] * data_copy[i - this->width - 1] + matrix[0][1] * data_copy[i - this->width] + matrix[0][2] * data_copy[i - this->width + 1] + 
			matrix[1][0] * data_copy[i - 1] + matrix[1][1] * data_copy[i] + matrix[1][2] * data_copy[i + 1] +
			matrix[2][0] * data_copy[i + this->width - 1] + matrix[2][1] * data_copy[i + this->width] + matrix[2][2] * data_copy[i + this->width + 1]) / 16;
	}
	double endTime = MPI_Wtime();

	#ifdef FULL_OUTPUT
		std::cout << "[Rank " << rank << "] Applied to following rows of pixels: " << range.first << " - " << range.second-1 << "\n";
		std::cout << "[Rank " << rank << "] Filter applied in: " << (endTime - startTime) << " s\n";
	#endif
	timeElapsed += endTime - startTime;
	
	if (rank != numberOfProcesses - 1)
	{
		MPI_Send(this->pixel, (toRow-1) * this->width, MPI_UNSIGNED_CHAR, rank+1, 17, MPI_COMM_WORLD);
		MPI_Send(&timeElapsed, 1, MPI_DOUBLE, rank+1, 0, MPI_COMM_WORLD);
	}
	else
    {
		std::cout << "Total time elapsed: " << timeElapsed << " s\n";
        std::cout << "Amdahl acceleration: " << 1 / ((endTime - startTime) / timeElapsed) << "\n";
    }
}

int Image::getWidth() { return this->width; }
int Image::getHeight() { return this->height; }
std::string Image::getFileName() { return this->fileName ;}
