#include <iostream>

#include "../include/image.h"
#include <chrono>
#include <mpi/mpi.h>

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
    int numberOfProcesses;
	int rank;

    MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcesses);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " filename.pbm\n";
        return 1;
    }

    std::string argsFileName = argv[1];
    auto dotPos = argsFileName.find(".");
    if (dotPos == std::string::npos)
    {
        std::cerr << "Usage: " << argv[0] << " filename.pbm\n";
        return 1;
    }

    std::string fileName = argsFileName.substr(0, dotPos); 
    std::string fileExtension = argsFileName.substr(dotPos);

    if (fileExtension.compare(".pgm") != 0)
    {
        std::cerr << "Incorrect file format.\n";
        return 1;
    }

    Image image;
    image.open(fileName + fileExtension);

    if (rank == 0)
    {
        std::cout << "[Rank " << rank << "] Opening file: "<< fileName + fileExtension << "\n";
        std::cout << "[Rank " << rank << "] Image size: " << image.getWidth() << "x" << image.getHeight() << "\n";
        std::cout << "[Rank " << rank << "] Applying Gaussian filter with 3x3 mask...\n";
    }

    image.applyFilter();


    if (rank == numberOfProcesses - 1)
        image.save(fileName + "_out" + fileExtension);

    MPI_Finalize();
}