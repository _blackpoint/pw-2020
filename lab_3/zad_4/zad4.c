#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "pomiar_czasu/pomiar_czasu.h"

#define THREADS_AMM 5
#define PTHREAD_STACK_MIN 16384

#define WYMIAR 1000
#define ROZMIAR WYMIAR *WYMIAR
double a[ROZMIAR], b[ROZMIAR], c[ROZMIAR];

void *heavy_task()
{
    int i, j, k;
    int n = WYMIAR;
    for (i = 0; i < ROZMIAR; i++)
        a[i] = 1.0 * i;
    for (i = 0; i < ROZMIAR; i++)
        b[i] = 1.0 * (ROZMIAR - i);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            c[i + n * j] = 0.0;
            for (k = 0; k < n; k++)
            {
                c[i + n * j] += a[i + n * k] * b[k + n * j];
            }
        }
    }
    double result = c[ROZMIAR - 1];
}

int main()
{
    pthread_t tid[THREADS_AMM];
    pthread_attr_t attr;

    size_t stack_size[5] = { 1          * PTHREAD_STACK_MIN, 
                             100        * PTHREAD_STACK_MIN, 
                             1000       * PTHREAD_STACK_MIN, 
                             10000     * PTHREAD_STACK_MIN,
                             100000   * PTHREAD_STACK_MIN };
    
    pthread_attr_init(&attr);
    for (int i = 0; i < THREADS_AMM; i++)
    {
        printf("=========================================\n");
        printf("Stack size: %ld\n", stack_size[i]);

        pthread_attr_setstacksize(&attr, stack_size[i]);

        inicjuj_czas();
        pthread_create(&tid[i], &attr, heavy_task, NULL);
        pthread_join(tid[i], NULL);
        drukuj_czas();
    }
}
