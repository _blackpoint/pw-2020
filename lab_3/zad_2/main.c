#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *thread_function(void *arg_wsk) 
{
    printf("Watek potomny nr: %d\n", *((int*) arg_wsk));
}

int main()
{
    pthread_t threads[10];
    
    for (int i = 0; i < 10; i++)
    {
        pthread_create(&threads[i], NULL, thread_function, &i);
    }

    for (int i = 0; i < 10; i++)
    {
        pthread_join(threads[i], NULL);
        printf("Konczenie watku potomnego nr: %d\n", i);
    }
}