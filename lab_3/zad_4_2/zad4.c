#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include <sched.h>

#include "pomiar_czasu/pomiar_czasu.h"

#define THREADS_AMM 4

#define WYMIAR 1000
#define ROZMIAR WYMIAR *WYMIAR
double a[ROZMIAR], b[ROZMIAR], c[ROZMIAR];

void *heavy_task(void *args)
{
    int i, j, k;
    int n = WYMIAR;
    for (i = 0; i < ROZMIAR; i++)
        a[i] = 1.0 * i;
    for (i = 0; i < ROZMIAR; i++)
        b[i] = 1.0 * (ROZMIAR - i);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            c[i + n * j] = 0.0;
            for (k = 0; k < n; k++)
            {
                c[i + n * j] += a[i + n * k] * b[k + n * j];
            }
        }
    }
    double result = c[ROZMIAR - 1];
}

int main()
{
    pthread_t tid[THREADS_AMM];
    cpu_set_t cpu[THREADS_AMM];
    pthread_attr_t attr[THREADS_AMM];

    for (int i = 0; i < THREADS_AMM; i++)
    {
        CPU_ZERO(&cpu[i]);
        CPU_SET(i, &cpu[i]);
        pthread_attr_init(&attr[i]);
        int status = pthread_attr_setaffinity_np(&attr[i], sizeof(cpu_set_t), &cpu[i]);

        if (status != 0) {
            printf("Error while assigning CPUs\n");
            exit(NULL);
        }
    }
    
    int status;
    
    for (int i = 0; i < THREADS_AMM; i++)
    {
        printf("=========================================\n");
        printf("CPU number: %d\n", i + 1);
        
        inicjuj_czas();
        pthread_create(&tid[i], &attr[i], heavy_task, NULL);        

        pthread_join(tid[i], NULL);
        drukuj_czas();
    }
}
