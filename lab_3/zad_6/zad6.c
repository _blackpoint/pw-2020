#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

#define THREAD_AMM 10
#define PTHREAD_STACK_MIN 16384

void *thread_task()
{
    printf("=================================\n");
    printf("THREAD WITH TID: %ld\n", pthread_self());
    pthread_attr_t attr;
    pthread_getattr_np(pthread_self(), &attr);

    size_t stack_size;
    pthread_attr_getstacksize(&attr, &stack_size);
    printf("Stack size: %ld\n", stack_size);

    /* 
     * PTHREAD_CREATE_DETACHED = 1
     * PTHREAD_CREATE_JOINABLE = 0
    */
    int detach_state;
    pthread_attr_getdetachstate(&attr, &detach_state);
    printf("Detach stade: %d\n", detach_state);

    /*  
     * PTHREAD_EXPLICIT_SCHED = 1
     * PTHREAD_INHERIT_SCHED = 0
     */ 
    int inherit_sched;
    pthread_attr_getinheritsched(&attr, &inherit_sched);
    printf("Inherited scheduling policy: %d\n", inherit_sched);

    /*
     * PTHREAD_SCOPE_PROCESS = 1
     * PTHREAD_SCOPE_SYSTEM = 0
     */
    int scope;
    pthread_attr_getscope(&attr, &scope);
    printf("Scope: %d\n", scope);
}

int main()
{
    pthread_t tid[THREAD_AMM];
    pthread_attr_t attr[THREAD_AMM];

    srand(time(NULL));

    for (int i = 0; i < THREAD_AMM; i++)
    {
        pthread_attr_init(&attr[i]);

        /*
         * attr functions
         * https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032j/index.html
        */

        // Setting random stack size
        size_t stack_size = PTHREAD_STACK_MIN * (rand() % 5 + 1);
        pthread_attr_setstacksize(&attr[i], stack_size);

        // Setting detachstate
        if (rand() % 2)
        {
            pthread_attr_setdetachstate(&attr[i], PTHREAD_CREATE_DETACHED);
        }
        else { pthread_attr_setdetachstate(&attr[i], PTHREAD_CREATE_JOINABLE); } 
        
        // Setting inheritedsched
        if (rand() % 2)
        {
            pthread_attr_setinheritsched(&attr[i], PTHREAD_EXPLICIT_SCHED);
        }

        // Setting scope
        if (rand() % 2) 
        {
            pthread_attr_setscope(&attr[i], PTHREAD_SCOPE_PROCESS);
        }
        else { pthread_attr_setscope(&attr[i], PTHREAD_SCOPE_SYSTEM); }

        int status = pthread_create(&tid[i], &attr[i], thread_task, NULL);

        if (status != 0) 
        {
            printf("Error while creating thread with TID: %ld", tid[i]);
        }
        
        sleep(1);
    }

    return 0;
}