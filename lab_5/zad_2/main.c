#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define ARRAY_SIZE 100 // Defines range where program will look for prime numbers (1 - ARRAY_SIZE)

// Global vars
int *input_array;
pthread_mutex_t mutex;

// Declarations
void display_array(int *arr, size_t size);
void eliminate_multiple(size_t index);
void *eliminate_task(void* args);

int main() 
{
    pthread_t threads[ARRAY_SIZE];
    pthread_mutex_init(&mutex, NULL);

    input_array = malloc(sizeof(int) * ARRAY_SIZE);

    for (size_t i = 0; i < ARRAY_SIZE; i++)
    {
        input_array[i] = i + 1;
    }

    printf("Input array:\n");
    display_array(input_array, ARRAY_SIZE);

    size_t thread_counter = 0;
    for (size_t i = 1; i < ARRAY_SIZE; i++)
    {
        if(input_array[i])
        {
            pthread_create(&threads[thread_counter], NULL, eliminate_task, (void*)i);
            thread_counter++;
        }
    }

    for (size_t i = 0; i < thread_counter; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Prime numbers in given range:\n");
    for (size_t i = 0; i < ARRAY_SIZE; i++)
    {
        if (input_array[i]) printf("%d ", input_array[i]);
    }
    printf("\n");

    free(input_array);
}

void *eliminate_task(void* args)
{
    eliminate_multiple((size_t)args);
}

void eliminate_multiple(size_t index)
{
    if (index + 1 >= ARRAY_SIZE) return;

    int base = input_array[index];
    for (size_t i = index + 1; i < ARRAY_SIZE; i++)
    {
        if(input_array[i] != 0 && input_array[i] % base == 0)
        {
            pthread_mutex_lock(&mutex);
            input_array[i] = 0;
            pthread_mutex_unlock(&mutex);
        }
    }
}

void display_array(int *arr, size_t size) 
{
    printf("Array with size %ld\n", size);

    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}