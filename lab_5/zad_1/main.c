#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define ARRAY_SIZE 1000000

// Global variables
int arr[ARRAY_SIZE];
int *arr_part1;
int *arr_part2;

// Functions declarations
void merge_sort(int arr[], size_t l, size_t r);
void part_merge(int arr[], size_t l, size_t m, size_t r);
void merge(int *arr_out, int *arr_part1, int *arr_part2);
void *merge_sort_ttask(void *args);
void split_array(int arr[], int **arr_out1, int **arr_out2);
void display_array(int *arr, size_t size);


int main()
{
    srand(time(NULL));
    clock_t start_time;
    clock_t end_time;
    double time_spent = 0.0;

    pthread_t threads[2];

    for (int i = 0; i < ARRAY_SIZE; i++)
    {
        arr[i] = rand() % 1000;
    }

    // printf("Unsorted array:\n");
    // display_array(arr, ARRAY_SIZE);
    // printf("----------------------------------\n");
    split_array(arr, &arr_part1, &arr_part2);

    printf("Sorting...\n");
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&threads[i], NULL, merge_sort_ttask, (void*)i); // casting with different sizes but works ok for now
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Merging...\n");
    merge(arr, arr_part1, arr_part2);

    printf("Sorted array:\n");
    display_array(arr, ARRAY_SIZE);


    free(arr_part1);
    free(arr_part2);
}

void merge(int *arr_out, int *arr_part1, int *arr_part2)
{
    size_t j = 0; // pointer for arr_part1
    size_t k = 0; // pointer for arr_part2

    for (size_t i = 0; i < ARRAY_SIZE; i++)
    {
        if (j >= ARRAY_SIZE / 2)
        {
            arr[i] = arr_part2[k];
            k++;
            continue;
        }

        if (k >= (ARRAY_SIZE + 1) / 2)
        {
            arr[i] = arr_part1[j];
            j++;
            continue;
        }

        if (arr_part1[j] < arr_part2[k])
        {
            arr_out[i] = arr_part1[j];
            j++;
        }
        else
        {
            arr_out[i] = arr_part2[k];
            k++;
        }
    }
}

void *merge_sort_ttask(void *args)
{
    int input = (int)args;
    if (input)
    {
        merge_sort(arr_part2, 0, ((ARRAY_SIZE + 1) / 2) - 1);
    }
    else 
    {
        merge_sort(arr_part1, 0, (ARRAY_SIZE / 2) - 1);
    }
}

void merge_sort(int arr[], size_t l, size_t r)
{
    if (l < r)
    {
        // Same as (l+r)/2, but avoids overflow for
        // large l and h
        size_t m = l + (r - l) / 2;

        // Sort first and second halves
        merge_sort(arr, l, m);
        merge_sort(arr, m + 1, r);

        part_merge(arr, l, m, r);
    }
}

void part_merge(int arr[], size_t l, size_t m, size_t r)
{
    size_t i, j, k;
    size_t n1 = m - l + 1;
    size_t n2 = r - m;

    int L[n1], R[n2];

    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void split_array(int arr[], int **arr_out1, int **arr_out2)
{
    size_t arr_size1 = ARRAY_SIZE / 2;
    size_t arr_size2 = (ARRAY_SIZE + 1) / 2;

    *arr_out1 = malloc(sizeof(int) * (arr_size1));
    *arr_out2 = malloc(sizeof(int) * (arr_size2));

    for (size_t i = 0; i < arr_size1; i++)
    {
        (*arr_out1)[i] = arr[i];
    }

    for (size_t i = 0; i < arr_size2; i++)
    {
        (*arr_out2)[i] = arr[arr_size1 + i];
    }
}

void display_array(int *arr, size_t size) 
{
    printf("Array with size %ld\n", size);

    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
